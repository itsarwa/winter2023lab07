import java.util.Scanner;

public class TicTacToeGame {
    public static void main(String[] args) {
        System.out.println("Welcome to Tic Tac Toe!");

        Board board = new Board();
        boolean gameOver = false;
        int player = 1;
        Square playerToken = Square.X;

        while (!gameOver) {
            System.out.println(board);

            if (player == 1) {
                playerToken = Square.X;
            } else {
                playerToken = Square.O;
       }

            int row, col;
            Scanner scanner = new Scanner(System.in);
			
            System.out.print("Player " + player + " (" + playerToken + "), enter row (1-3): ");
			row = scanner.nextInt() - 1;
			
			System.out.print("Player " + player + " (" + playerToken + "), enter column (1-3): ");
			col = scanner.nextInt() - 1;


            while (!board.placeToken(row, col, playerToken)) {
			System.out.println("Invalid input! Please try again.");
			
			System.out.print("Player " + player + " (" + playerToken + "), enter row (1-3): ");
			row = scanner.nextInt() - 1;
			
			System.out.print("Player " + player + " (" + playerToken + "), enter column (1-3): ");
			col = scanner.nextInt() - 1;

       }

            if (board.checkIfFull()) {
                System.out.println("It's a tie!");
                gameOver = true;
				
            } else if (board.checkIfWinning(playerToken)) {
              System.out.println("Player " + player + " wins!");
			gameOver = true;
				
            } else {
                player++;
                if (player > 2) {
                    player = 1;
                }
            }
        }
    }
}
