public class Board1 {
    private Square[][] tictactoeBoard;

    public Board() {
        tictactoeBoard = new Square[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                tictactoeBoard[i][j] = Square.BLANK;
            }
        }
    }

   
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                sb.append(tictactoeBoard[i][j].toString());
                if (j < 2) {
                    sb.append(" ");
                }
            }
            if (i < 2) {
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    public boolean placeToken(int row, int col, Square playerToken) {
        if (row < 0 || row > 2 || col < 0 || col > 2) {
            return false;
        }
        if (tictactoeBoard[row][col] != Square.BLANK) {
            return false;
        }
        tictactoeBoard[row][col] = playerToken;
        return true;
    }

    public boolean checkIfFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (tictactoeBoard[i][j] == Square.BLANK) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkIfWinningHorizontal(Square playerToken) {
        for (int i = 0; i < 3; i++) {
            if (tictactoeBoard[i][0] == playerToken && tictactoeBoard[i][1] == playerToken && tictactoeBoard[i][2] == playerToken) {
                return true;
            }
        }
        return false;
    }

    private boolean checkIfWinningVertical(Square playerToken) {
        for (int i = 0; i < 3; i++) {
            if (tictactoeBoard[0][i] == playerToken && tictactoeBoard[1][i] == playerToken && tictactoeBoard[2][i] == playerToken) {
                return true;
            }
        }
        return false;
    }

    public boolean checkIfWinning(Square playerToken) {
        if (checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken)) {
            return true;
        }
        return false;
    }
}
