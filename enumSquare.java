public enumSquare {
    X("X"),
    O("O"),
    BLANK("_");

    private final String displayValue;

    Square(String displayValue) {
        this.displayValue = displayValue;
    }

    public String toString() {
        return displayValue;
    }
}
